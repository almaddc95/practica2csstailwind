/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [],
  theme: {
    extend: {
      boxShadow: {
        '3xl': '8px 8px 0px 0px rgba(0, 0, 0, 1)',
        '4xl': [
          '0 35px 35px rgba(0, 0, 0, 0.25)',
          '0 45px 65px rgba(0, 0, 0, 0.15)'
      ]
      },
      colors: {
        primarioyellow: '#f4d04e',
        colorwhite:' #ffffff',
        colorgrey: '#808080',
        colorblack: '#121212',
      },
      fontFamily:{
        figtree :  ['Figtree', 'sans-serif'],
        // protest  :  ['Protest Revolution'],
      },
    },
  },
  plugins: [],
}

